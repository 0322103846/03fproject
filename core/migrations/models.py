from django.db import models
from django.contrib.auth.models import User
 
# Create your models here.
class imagen (models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    link=models.ImageField(null=True, blank=True)
    link2=models.ImageField(null=True, blank=True)
    link3=models.ImageField(null=True, blank=True)
    def __str__(self):
        return self.name

class camping (models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre=models.CharField(max_length=30, default='generico')
    year=models.DateField(auto_now_add=True)
    def __str__(self):
        return self.name

class gender(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=10, default='nonbinary')
    def __str__(self):
        return self.name

class category(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=25, default='generic caegory')
    def __str__(self):
        return self.name

class payment(models.Model):
    name=models.CharField(max_length=10)
    reference=models.CharField(max_length=8)
    def __str__(self):
        return self.name

class custormer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name_first=models.CharField(max_length=35)
    last_name_pat=models.CharField(max_length=25)
    last_name_mat=models.CharField(max_length=25, blank=True)
    phone=models.CharField(max_length=15)
    cp=models.CharField(max_length=5)
    streat=models.CharField(max_length=25)
    number_home=models.CharField(max_length=3)
    def __str__(self):
        return self.name


class order(models.Model):
    payment=models.ForeignKey(payment, on_delete=models.CASCADE)
    custormer=models.ForeignKey(custormer, on_delete=models.CASCADE)
    date=models.DateField(auto_now_add=True)
    code_flow=models.CharField(max_length=10)
    subtotal=models.FloatField(default=0.0)
    IVA=models.FloatField(default=0.0)
    total=models.FloatField(default=0.0)
    def __str__(self):
        return self.name

class product(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    imagen=models.ForeignKey(imagen, on_delete=models.CASCADE)
    compaing=models.ForeignKey(camping, on_delete=models.CASCADE)
    gender=models.ForeignKey(gender, on_delete=models.CASCADE)
    category=models.ForeignKey(category, on_delete=models.CASCADE)
    name=models.CharField(max_length=35)
    description=models.TextField(max_length=256)
    size=models.CharField(max_length=4)
    price_uni=models.FloatField(default=0)
    status=models.BooleanField(default=False)
    stock=models.IntegerField(default=1)
    slug=models.SlugField(default="Generic slug", max_length=6)
    def __str__(self):
        return self.name